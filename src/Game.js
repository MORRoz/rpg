export class Game {
    constructor() {
        this.gameLevel = 1;
        this.enemyAmount = 2;
        this.bottleCount = 0;
        this.showGate = false;
    }
}
