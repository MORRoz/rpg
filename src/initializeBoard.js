import { boardHeight, gameBoard, boardWidth } from './javascript';
export function initializeBoard() {
    for (let k = 0; k < boardHeight; k++) {
        gameBoard.push([]);
        for (let i = 0; i < boardWidth; i++) {
            gameBoard[k].push(" ");
        }
    }
}
