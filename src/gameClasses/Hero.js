import {} from '../javascript';
export class HeroClass {
    constructor() {
        this.x = 1;
        this.y = 1;
        this.attack = 5;
        this.hp = 60;
        this.exp = 0;   
        this.expToUpdate = 0;     
    }
}
