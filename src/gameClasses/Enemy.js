import {randomValue} from '../mathFunctions/random';


export class Enemy {
    constructor(gameLevel) {
        this.x = randomValue(0, 9);
        this.y = randomValue(0, 9);
        if (gameLevel > 2) {
            this.type = 'priest';
            this.hp = 40;
            this.originalHP = 40;
            this.strength = 15;
        } else {
            this.type = 'spider';
            this.hp = 20;
            this.originalHP = 20;
            this.strength = 10;
        }
        
    }
}
