import { randomValue } from '../mathFunctions/random';
import { bottles } from '../javascript';
export class Bottle {
    constructor() {
        this.x = randomValue(0, 9);
        this.y = randomValue(0, 9);
        this.hp = randomValue(20, 40);
        bottles.push(this);
    }
}
