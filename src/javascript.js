import './style.css';
import imgGrass from './img/grass.jpg';
import imgKnight from './img/knight.png';
import imgPriest from './img/priest.png';
import imgSpider from './img/spider_small.png';
import imgBottle from './img/bottle.png';
import imgGate from './img/gates_small.png';

import { HeroClass } from './gameClasses/Hero';
import { renderScoreBoard } from './render/renderScoreBoard';
import { Bottle } from './gameClasses/Bottle';
import { renderBottles } from './render/renderBottles';
import { renderEnemies } from './render/renderEnemies';
import { renderEnemyHP } from './render/renderEnemyHP';
import { renderFieldGrass } from './render/renderFieldGrass';
import { Game } from './Game';
import { createObjects } from './createObjects';

var buttonUp = document.getElementById('buttonUp');
var buttonDown = document.getElementById('buttonDown');
var buttonLeft = document.getElementById('buttonLeft');
var buttonRight = document.getElementById('buttonRight');
var buttonAttack = document.getElementById('buttonAttack');


export var canvas = document.getElementById('myCanvas');
export var ctx = canvas.getContext('2d');
ctx.font = "20px Arial";

var canvasScore = document.getElementById('scoreScreenCanvas');
export var ctxScore = canvasScore.getContext('2d');
ctxScore.font = "24px sans-serif";

export var boardHeight = 10;
export var boardWidth = 10;

export var hero;

export var enemies = [];
export var bottles = [];

export var gameBoard = [];

export var game;

export var grassImage = new Image();
grassImage.onload = function () {

    renderFieldGrass();
}
grassImage.src = "img/grass.jpg";

var knightImage = new Image();
knightImage.onload = function () {
    ctx.drawImage(knightImage, 20 + hero.x * 30, 0 + hero.y * 30, 40, 40);
}
knightImage.src = 'img/knight.png';

export var priestImage = new Image();
priestImage.onload = function () {
    renderEnemies();
};
priestImage.src = 'img/priest.png';

export var spiderImage = new Image();
spiderImage.onload = function () {
    renderEnemies();
};
spiderImage.src = 'img/spider_small.png';

export var bottleIMage = new Image();
bottleIMage.onload = function () {
    renderBottles();
};
bottleIMage.src = 'img/bottle.png';

export var gateImage = new Image();
gateImage.onload = function () {
    renderGate();
};
gateImage.src = 'img/gates_small.png';

function renderGate() {     
    if (game.showGate === true) {
        ctx.drawImage(gateImage, 295, 280, 30, 30);
    }
}


function render() {
    ctx.font = "20px Arial";
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    renderFieldGrass();

    ctx.drawImage(knightImage, 20 + hero.x * 30, 0 + hero.y * 30, 40, 40);
    renderEnemies();

}

function deleteEnemy(toDelete) {

    for (let i = 0; i < enemies.length; i++) {
        if (enemies[i] === toDelete) {
            enemies.splice(i, 1);
            i--;
        }
    }
}

function deleteBottle(toDelete) {

    for (let i = 0; i < bottles.length; i++) {
        if (bottles[i] === toDelete) {
            bottles.splice(i, 1);
            i--;
        }
    }
}

function startGame() {

    game = new Game();
    hero = new HeroClass();
    createObjects();
}



function nextGameStep() {

    if (game.levelUp === true) {  
        game.showGate = false;       
        game.gameLevel++;
        hero.hp += 60;
        createObjects();
    }

    if (game.gameLevel === 1) {
        if (enemies.length === 1) {
            var bottle = new Bottle;
            bottles.push(bottle);                   
        }
    } else if (game.gameLevel === 2) {
        if (enemies.length === 2) {
            var bottle = new Bottle;
            bottles.push(bottle);
        }    
    } else if (game.gameLevel === 3) {
        if (enemies.length === 2) {
            var bottle = new Bottle;
            bottles.push(bottle);
        } 
    }

    if (enemies.length === 0) {
        game.showGate = true;             
    }
    
    if (hero.expToUpdate > 49) {        
        hero.attack += 10;
        hero.expToUpdate -= 50;
    }
}


startGame();

function badMove(x, y) {
    let badMoveLocal = false;

    if ((x < 0 || x > boardWidth - 1) || (y < 0 || y > boardHeight - 1)) {
        badMoveLocal = true;
    }

    enemies.forEach(enemyItem => {
        if (x == enemyItem.x && y == enemyItem.y) { badMoveLocal = true }
    }
    );
    return badMoveLocal;
}

function canNotAttack() {

    var arrayOfEnemies = [];

    for (let i = 0; i < enemies.length; i++) {
        if ((((Math.abs(hero.x - enemies[i].x) > 1) || (Math.abs(hero.y - enemies[i].y) > 1))) == false) {
            arrayOfEnemies.push(enemies[i]);
        }
    }
    return arrayOfEnemies;
}

buttonUp.onclick = function () {
    moveHero(0, -1);
}

buttonDown.onclick = function () {
    moveHero(0, 1);
}

buttonLeft.onclick = function () {
    moveHero(-1, 0);
}

buttonRight.onclick = function () {
    moveHero(1, 0);
}

export function renderGame() {

    render();
    renderBottles();
    renderGate();
    renderScoreBoard();
    renderEnemyHP();

}

function moveHero(xx, yy) {
    if (badMove(hero.x + xx, hero.y + yy)) return;

    hero.x += xx;
    hero.y += yy;

    makeInteractionsWithItems();
    renderGame();

}

function makeInteractionsWithItems() {
    bottles.forEach(bottleItem => {
        if (bottleItem.x === hero.x && bottleItem.y === hero.y) {
            hero.hp += bottleItem.hp;
            deleteBottle(bottleItem);
        }
    });

    if (game.showGate === true) {
         
        if (hero.x === 9 && hero.y === 9) {
            game.levelUp = true;
            nextGameStep();

        }
    }

}

buttonAttack.onclick = function () {
    attack();
};

function attack() {

    var arrayOfCloseEnemies = canNotAttack();

    if (arrayOfCloseEnemies.length == 0) {
        return;
    }

    for (let i = 0; i < arrayOfCloseEnemies.length; i++) {
        var enemyTmp = arrayOfCloseEnemies[i];
        enemyTmp.hp -= hero.attack;
        hero.hp -= enemyTmp.strength;

        if (hero.hp > 0) {
            renderScoreBoard();
            renderEnemyHP();

            if (enemyTmp.hp <= 0) {
                deleteEnemy(enemyTmp);
                hero.exp += enemyTmp.originalHP;
                hero.expToUpdate += enemyTmp.originalHP;


                nextGameStep();
                renderGame();
            }
        } //game over
        else {
            renderScoreBoard();
            ctx.font = "50px Arial";
            ctx.fillText("Game over", 50, 150);
            setTimeout(startGame, 4000);

        }
    }
}