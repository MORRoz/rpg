import { Enemy } from './gameClasses/Enemy';
import { initializeBoard } from './initializeBoard';
import { hero, game, enemies, renderGame } from './javascript';
export function createObjects() {
    hero.x = 1;
    hero.y = 1;
    if (game.gameLevel === 1) {
        game.enemyAmount = 2;
        game.bottleCount = 0;
    }
    else if (game.gameLevel === 2) {
        game.enemyAmount = 3;
        game.bottleCount = 0;
    }
    else if (game.gameLevel === 3) {
        game.enemyAmount = 4;
        game.bottleCount = 0;
    }
    else if (game.gameLevel === 4) {
        game.enemyAmount = 5;
        game.bottleCount = 0;
    }
    else if (game.gameLevel === 5) {
        game.enemyAmount = 5;
        game.bottleCount = 0;
    }
    game.showGate = false;
    game.levelUp = false;
    initializeBoard();

    for (let i = 0; i < game.enemyAmount; i++) {
        var enemy = new Enemy(game.gameLevel);
        enemies.push(enemy);
    }
    renderGame();
}
