import { ctx, grassImage } from '../javascript';
export function renderFieldGrass() {
    var leftPositionMainBlock = 256;
    var heightPositionMainBlock = 169;
    var blockSize = 90;
    var drawSize = 30;
    var leftPositionCornerLeftBlock = 40;
    var heightPositionCornerLeftBlock = 20;
    ctx.drawImage(grassImage, 40, 20, 90, 90, 5, 0, 30, 30);
    ctx.drawImage(grassImage, 470, 20, 90, 90, 305, 0, 30, 30);
    for (let i = 1; i < 10; i++) {
        ctx.drawImage(grassImage, 256, 20, 90, 90, 5 + i * 30, 0, 30, 30);
    }
    for (let k = 1; k < 9; k++) {
        for (let i = 0; i <= 10; i++) {
            if (i == 0) {
                leftPositionMainBlock = 40;
            }
            else {
                var leftPositionMainBlock = 256;
            }
            ;
            if (i == 10) {
                leftPositionMainBlock = 470;
            }
            ;
            ctx.drawImage(grassImage, leftPositionMainBlock, heightPositionMainBlock, blockSize, blockSize, 5 + i * drawSize, k * drawSize, drawSize, drawSize);
        }
    }
    for (let i = 1; i < 10; i++) {
        ctx.drawImage(grassImage, 256, 320, 90, 90, 5 + i * 30, 30 * 9, 30, 30);
    }
    ctx.drawImage(grassImage, 40, 320, 90, 90, 5, 270, 30, 30);
    ctx.drawImage(grassImage, 470, 320, 90, 90, 305, 270, 30, 30);
}
