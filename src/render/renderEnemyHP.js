import { enemies, ctx } from '../javascript';
export function renderEnemyHP() {
    for (let i = 0; i < enemies.length; i++) {
        let hp = enemies[i].hp;
        let x = enemies[i].x;
        let y = enemies[i].y;
        ctx.clearRect(45 + x * 30, 4 + y * 30, 14, 14);
        ctx.beginPath();
        ctx.stroke();
        ctx.font = "12px Arial";
        ctx.fillText(hp, 45 + x * 30, 15 + y * 30);
    }
}
