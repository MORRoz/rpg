import { enemies, ctx, priestImage, spiderImage } from '../javascript';
export function renderEnemies() {
    for (let i = 0; i < enemies.length; i++) {
        if (enemies[i].type === 'priest') {
            ctx.drawImage(priestImage, 25 + enemies[i].x * 30, 10 + enemies[i].y * 30, 20, 30);
        } else {
            ctx.drawImage(spiderImage, 25 + enemies[i].x * 30, 10 + enemies[i].y * 30, 20, 30);
        }    
    }
}
