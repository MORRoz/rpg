import { bottles, ctx, bottleIMage } from '../javascript';
export function renderBottles() {
    bottles.forEach(bottleElement => ctx.drawImage(bottleIMage, 25 + bottleElement.x * 30, bottleElement.y * 30, 25, 30));
}
