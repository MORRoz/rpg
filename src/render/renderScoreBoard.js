import { ctxScore, canvas, hero, game } from '../javascript';
export function renderScoreBoard() {
    ctxScore.clearRect(0, 0, canvas.width, canvas.height);
    ctxScore.fillText("HP : ", 45, 25);
    if (hero.hp <= 20) {
        ctxScore.fillStyle = '#eb3110';
    }
    else if (hero.hp <= 40) {
        ctxScore.fillStyle = '#e6ed24';
    }
    ctxScore.fillText(hero.hp, 95, 25);
    ctxScore.fillStyle = '#000000';
    ctxScore.fillText("Level : " + game.gameLevel, 45, 55);
    ctxScore.fillText("EXP : " + hero.exp, 200, 25);
    ctxScore.fillText("Attack : " + hero.attack, 200, 55);
    ctxScore.beginPath();
    ctxScore.rect(0, 0, 340, 70);
    ctxScore.stroke();
}
